"""
In this script I'll try to illustrate the initial local maxima which serves a 
first candidates for the sift method
"""

import numpy as np
import matplotlib.pyplot as plt
from sift import SIFT
import sift
from skimage.data import astronaut, coffee
from skimage.io import imread
import cv2
import pickle


plt.rcParams['image.cmap']= 'gray'


if __name__ == "__main__":
    
    #reading the image
    img = astronaut()
    #Sift detector
    # the constructor compute the dog pyramid and extract the local maxima
    detector = SIFT(img)

    candidates = detector.local_extremums()
    with open('candidates.pickle','wb') as F:
        pickle.dump(candidates, F)
    
    # with open('candidates.pickle','rb') as F:
    #     candidates = pickle.load(F)

    
    
    keypoints = detector._candidates_to_cv_keys(candidates)
    
    local_extremas = cv2.drawKeypoints(img, keypoints,outImage = np.array([])\
            ,color= (0,0,255),flags= cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    plt.show()


    ########################################
    #  Discarding low contrast candidates  #
    ########################################
    
    candidates = sift.discard_low_contrast(detector.DoG, candidates)

    keypoints = detector._candidates_to_cv_keys(candidates)
    
    low_constrast1 = cv2.drawKeypoints(img, keypoints,outImage = np.array([])\
            ,color= (0,0,255),flags= cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    
    ##############################
    #  Key points interpolation  #
    ##############################
    candidates_interp = sift.keypoints_interpolation(detector.DoG, candidates)
    keypoints = detector._detailled_candidates_to_cv_keys(candidates_interp)
    
    interpolated = cv2.drawKeypoints(img, keypoints,outImage = np.array([])\
            ,color= (0,0,255),flags= cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)


    #################################
    #  Discard condidates on edges  #
    #################################
    candidates_non_edges = sift.discard_edges_candidates(detector.DoG,
            candidates_interp)
    
    keypoints = detector._detailled_candidates_to_cv_keys(candidates_non_edges)
    
    non_edges = cv2.drawKeypoints(img, keypoints,outImage = np.array([])\
            ,color= (0,0,255),flags= cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    #############
    #  Figures  #
    #############
    fig, axs = plt.subplots(1,4, figsize=(16,4))
    #local maximas
    axs[0].imshow(local_extremas)
    axs[0].set_title("local maximas")

    #low contrast
    axs[1].imshow(low_constrast1)
    axs[1].set_title("Removing low contrast")

    # interpoloated
    axs[2].imshow(interpolated)
    axs[2].set_title("Keypoint interpolation")


    # non edges
    axs[3].imshow(non_edges)
    axs[3].set_title("removing edges")
    plt.show()
