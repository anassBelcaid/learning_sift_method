########################################
#  Testing the interpolation function  #
########################################

#########################################
#  Module to implement the sift method  #
#########################################

import numpy as np
import matplotlib.pyplot as plt
from time import time
from skimage.data import coffee
from skimage.transform import rescale
import warnings
warnings.filterwarnings('ignore')



def test_interpolation():

    #loading an initial image
    img  = coffee()

    #interpolating the image
    # t = time()
    # img_2 = sift.bilinear_interpolation(img, 0.5)
    # t = time() - t
    # print("interpolating took {:.2f} second".format(t))
    # img_2 = rescale(img,scale=2)
    #plotting the image
    fig, axs = plt.subplots(1,2)
    axs[0].imshow(img)
    axs[0].axis('off')
    # axs[1].imshow(img_2)
    axs[1].axis('off')
    plt.show()

if __name__ == "__main__":
    
    test_interpolation()


