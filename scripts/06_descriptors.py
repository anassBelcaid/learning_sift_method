

"""
In this script we will finish the sift descrptors which tries to 
construct a local histogram of each keypoint
"""

import numpy as np
import matplotlib.pyplot as plt
from sift import SIFT
import sift
from skimage.data import astronaut
from skimage.io import imread
import cv2
import pickle
from skimage.transform import resize


###################
#  General setup  #
###################
import warnings
warnings.filterwarnings(action='ignore')
plt.rcParams['image.cmap'] = 'gray'

##################
#  Main program  #
##################
if __name__ == "__main__":
    #reading the image
    img = astronaut()


    #Sift detector
    # the constructor compute the dog pyramid and extract the local maxima
    detector = SIFT(img)


    # Computing the candidates
    # detector.detect()
    # with open("candidates.pickle","wb") as F:
    #     pickle.dump(detector.candidates, F)

    #loading the candidates
    with open("candidates.pickle", "rb") as F:
        detector.candidates = pickle.load(F)


    # Computing the histogram accumulation
    detector.orientation_accumulation()

    detector.describe()

    keypoints = detector._oriented_candidates_to_cv_keys(detector.candidates)
    orientations = cv2.drawKeypoints(img, keypoints,outImage = np.array([])\
            ,color= (0,0,255),flags= cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    plt.imshow(orientations)
    plt.show()
