import numpy as np
import matplotlib.pyplot as plt
from skimage.data import astronaut
from skimage.color import rgb2gray
import cv2


if __name__ == "__main__":
    
    #reading the image
    img = astronaut()

    #Creating a simple blob detector
    detector = cv2.SimpleBlobDetector_create()
    keypoints = detector.detect(img)

    image_with_keypoints = cv2.drawKeypoints(img, keypoints,outImage = np.array([])\
            ,color= (0,0,255),flags= cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    plt.imshow(image_with_keypoints)
    plt.show()

