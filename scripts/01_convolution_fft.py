######################################################################
#  Script pour illustrer l'utilisation de la transformée de fourier  #
# pour un calcul rapide du produit de convolution. L'algorithme est le suivant:

# 1- Soit un signal f(k) de taille N_1 et un noyau de taille N_2.
# 2- Padding le premier signal avec N_2 - 1 zeros
# 3- Padding le deuxième signal avec N1 - 1 zeros
# 4- Calculer les transofrmées de fourier de f = F(k) et celle de g=G(k)
# 5- Produit par élément des transformée de fourier Z(k) = F(k) . G(k)
# 6- Revenir dans le monde temporel avec l'inverse de fourier z[k] =   IFFT(Z)
######################################################################



import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":
    
    #définir un simple signal
    f = np.hstack((np.ones(3), 2*np.ones(3), 3* np.ones(3), 4*np.ones(3)))
    N1 = len(f)
    #Noyau
    g = np.array([1,2,3,4,5,6])
    N2 = len(g)

    #padding the signal
    f_padded = np.hstack((f, np.zeros( N2-1)))
    g_padded =  np.hstack((g, np.zeros(N1-1)))


    # Calculer les transofrmées de Fourier

    F = np.fft.fft(f_padded)
    G = np.fft.fft(g_padded)


    # Revenir au monde temporel
    Z  = np.real(np.fft.ifft(F*G))


    #convolution by filtering
    Z_conv = np.convolve(f, g)


    #Assert for equality
    assert(np.allclose(Z, Z_conv)), "Difference between dft and convolution"

    #representation graphique
    fig,ax = plt.subplots(2,2,sharex=True, sharey = False)
    ax[0][0].stem(f)
    ax[0][0].set_title('signal initial')

    ax[0][1].stem(g)
    ax[0][1].set_title('Noyau')

    ax[1][0].stem(Z_conv)
    ax[1][0].set_title('Simple Convolution')
    ax[1][1].stem(Z)
    ax[1][1].set_title('Convolution by FFT')

    plt.show()
