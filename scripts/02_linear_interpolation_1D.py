################################################################################
#         Script for linear interpoloation using the following method
#   1- Nearest neighbor
#   2- Linear interpolation
#   3- Gaussian interpolation
################################################################################

import numpy as np
import matplotlib.pyplot as plt


def gaussian_kernel(sigma,num_points=6):
    """
    retun a gaussian kernel of size 2*L+1 with std sigma
    """
    L = np.int(4*sigma)
    X = np.linspace(-L, L, 2*num_points+1)

    return 1/(np.sqrt(2*np.pi)*sigma) * np.exp(-X**2/sigma**2)

def signal(x):
    """
    simple signal to interpolate
    """

    return 2*x**3 - 2*x**2 + 3*x -1


def upsample(x,sig, factor=2,method='lin'):
    """
    interpoloate the signal sig with factor 
    """

    n = len(sig)
    
    # resulting signal
    y = np.zeros(n * factor)

    #copying the initial points
    y[::factor] = sig

    #convolving with a kernel

    if method == 'lin':
        kernel = (1/factor)*np.convolve(np.ones(factor), np.ones(factor))
        y = np.convolve(y,kernel, mode='same')

    if method == 'sinc':
        kernel = np.sinc(np.linspace(-factor, factor, 7))
        y = np.convolve(y, kernel, mode='same')
    
    if method == 'nearest':
        kernel = np.ones(2*factor)
        y = np.convolve(y, kernel, mode='same')
    if method == 'gauss':
        kernel = gaussian_kernel(0.8)
        y = np.convolve(y, kernel, mode='same')
    #interpolont points
    x1 = np.linspace(np.min(x), np.max(x), factor*n)
    x1[::factor] = x
    return x1, y

if __name__ == "__main__":
    
    #interpoloation points

    L = 3              # lenght of the kernel 2*L + 1
    n = 5              # intervale of interpoloaiton [-n, n]
    x = np.linspace(-n, n, 100)
    X = np.linspace(-n, n, 100)


    #points
    points = np.linspace(-n, n, 7)
    images = signal(points)



    #upsampling
    x1, y1 = upsample(points, images, L)

    #figure
    fig, axs = plt.subplots(4,2)

    #initial signal
    ax = axs[0,0]
    kernel = np.convolve(np.ones(L), np.ones(L))

    ax.plot(kernel,'r')
    ax.set_title("Linear kernel kernel")

    ax = axs[0,1]
    ax.plot(X,signal(X),alpha=0.3)
    ax.plot(points,images, 'bo',alpha=0.8)
    ax.plot(x1, y1, 'ro',alpha=0.5)

    #initial signal
    ax = axs[1,0]
    ax.plot(x,np.sinc(x),'r')
    ax.set_title("sinc kernel")

    x2, y2 = upsample(points, images, L, 'sinc')
    ax = axs[1,1]
    ax.plot(X,signal(X),alpha=0.3)
    ax.plot(points,images, 'bo',alpha=0.8)
    ax.plot(x2, y2, 'ro',alpha=0.5)

    #initial signal
    ax = axs[2,0]
    
    kernel = np.ones(2*L+1)
    ax.plot(np.linspace(-L,L+1,2*L+1),kernel,'r')
    ax.set_xlim(-6*L,6*L)
    ax.set_title("nearest")

    x3, y3 = upsample(points, images, L, 'nearest')
    ax = axs[2,1]
    ax.plot(X,signal(X),alpha=0.3)
    ax.plot(points,images, 'bo',alpha=0.8)
    ax.plot(x3, y3, 'ro',alpha=0.5)



    #initial signal
    ax = axs[3,0]
    
    kernel = gaussian_kernel(0.8)
    ax.plot(kernel,'r')
    ax.set_title("gaussian")

    x4, y4 = upsample(points, images, L, 'gauss')
    ax = axs[3,1]
    ax.plot(X,signal(X),alpha=0.3)
    ax.plot(points,images, 'bo',alpha=0.8)
    ax.plot(x4, y4, 'ro',alpha=0.5)
    plt.show()

